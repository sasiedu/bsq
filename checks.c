/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 19:10:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/18 11:23:32 by meckhard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		valid_map(void)
{
	int	i;
	int	j;
	int	nb;

	i = 0;
	while (g_str1[i] != '\n')
		i++;
	j = i + 1;
	while (g_str1[j] != '\n')
		j++;
	nb = j - (1 + i);
	if (ft_compare(nb, i + 1) == -1)
		return (-1);
	return (0);
}

int		ft_compare(int nb, int k)
{
	int	j;

	j = 1;
	while (g_str1[k])
	{
		if (g_str1[k] == '\n' || g_str1[k] == '\0')
		{
			if (j != nb + 1)
				return (-1);
			j = 0;
		}
		j++;
		k++;
	}
	return (0);
}

int		set_map_val(t_map *map1)
{
	int		i;
	int		j;
	char	*temp;

	i = 0;
	j = 0;
	temp = (char*)malloc(sizeof(char) * 6);
	while (g_str1[i] != '\n')
	{
		if (g_str1[i] >= '0' && g_str1[i] <= '9')
		{
			temp[j] = g_str1[i];
			j++;
		}
		i++;
	}
	j = i + 1;
	while (g_str1[j] != '\n')
		j++;
	map1->col = j - (i + 1);
	map1->lines = ft_atoi(temp);
	map1->sq = g_str1[i - 1];
	map1->obs = g_str1[i - 2];
	map1->free = g_str1[i - 3];
	return (0);
}

int		ft_from_file(char **argv, int i)
{
	int	size;

	if (open(argv[i], O_RDONLY) == -1)
		return (-1);
	size = ft_map_size(argv, i);
	read_map(argv, i, size);
	if (g_str1[0] > 57 || g_str1[0] < 48)
		return (-1);
	if (valid_map() == -1)
		return (-1);
	return (0);
}

void	ft_file(int i, char **argv)
{
	int		**grid;
	t_map	*map1;

	if (ft_from_file(argv, i) == -1)
	{
		write(1, "map error\n", 10);
		free(g_str1);
	}
	else
	{
		map1 = (t_map*)malloc(sizeof(t_map));
		set_map_val(map1);
		grid = ft_create_grid(map1);
		ft_solve(grid, map1);
		ft_free(map1, grid);
	}
}
